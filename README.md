# misrag-task

## Getting started

This is a project created for handling csv files analysis and answering questions about there content.

## How does it work

The firs thing is to create an OpenAi assistant that is responsible for handling the file processing, analyzing and answering questions about the file content.

## Frameworks and libraries

### Front-end

React ui library with MUI components for consistent styling.

### Back-end

Node js.

Multer: for file uploading.

RedisDB: for storing temp data on the server as a cash.

## How to run the project

1. Clone the project to your laptop.

2. Install the required libraries in both the Front-end directory app and the Back-end app by running `npm i` command.

3. Create an api key in the settings of your [openai](https://platform.openai.com/) account and use it in the .env file of the Back-end app.

4. Now run both the Front-end server (`npm run dev`) and the Back-end server (`node index.js`).

5. Open your browser to [localhost:5174](http://localhost:5174/) and start uploading your first file to the OpenAi assistant.
