import React, { useState } from "react";
import axios from "axios";
import {
  Container,
  AppBar,
  Toolbar,
  Typography,
  TextField,
  Button,
  List,
  ListItem,
  ListItemText,
  Paper,
  CircularProgress,
  IconButton,
  Dialog,
  DialogTitle,
  DialogContent,
  LinearProgress,
  Box,
} from "@mui/material";
import AttachFileIcon from "@mui/icons-material/AttachFile";

interface Message {
  text: string;
  sender: "user" | "bot";
}

const BASE_URL = import.meta.env.VITE_BASE_URL;

const App: React.FC = () => {
  const [input, setInput] = useState<string>("");
  const [messages, setMessages] = useState<Message[]>([]);
  const [uploading, setUploading] = useState<boolean>(false);
  const [uploadProgress, setUploadProgress] = useState<number>(0);

  const handleMessageSend = async () => {
    if (input.trim() !== "") {
      setMessages([...messages, { text: input, sender: "user" }]);
      setInput("");

      try {
        const response = await axios.post(`${BASE_URL}/message`, {
          message: input,
        });
        const data = await response.data;
        console.log(data);
        setMessages((messages) => [
          ...messages,
          { text: `${response.data.message}`, sender: "bot" },
        ]);
      } catch (error) {
        console.error("Error making POST request:", error);
      }
      setTimeout(() => {}, 500);
    }
  };

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (!file) return;

    const formData = new FormData();
    formData.append("file", file);

    const config = {
      onUploadProgress: (progressEvent: any) => {
        const percentCompleted = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total
        );
        setUploadProgress(percentCompleted);
      },
    };

    setUploading(true);
    axios
      .post(`${BASE_URL}/upload`, formData, config)
      .then((response) => {
        console.log(response);
        setMessages((messages) => [
          ...messages,
          {
            text: `File uploaded successfully: ${response.data}`,
            sender: "bot",
          },
        ]);
        setUploading(false);
      })
      .catch((error) => {
        setMessages((messages) => [
          ...messages,
          { text: `Error uploading file: ${error.message}`, sender: "bot" },
        ]);
        setUploading(false);
      });
  };

  return (
    <>
      <Container maxWidth="sm">
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6">ChatGPT Assistant</Typography>
          </Toolbar>
        </AppBar>

        <List>
          {messages.map((message, index) => (
            <ListItem key={index}>
              <ListItemText
                primary={message.text}
                align={message.sender === "user" ? "right" : "left"}
                primaryTypographyProps={{
                  component: Paper,
                  elevation: 3,
                  style: {
                    padding: "10px",
                    borderRadius: "10px",
                    maxWidth: "75%",
                    overflowWrap: "break-word",
                  },
                }}
              />
            </ListItem>
          ))}
        </List>

        <div
          className="wrapper"
          style={{ display: "flex", alignItems: "center", marginTop: "20px" }}
        >
          <div
            style={{
              flexGrow: 1,
              marginRight: "10px",
              display: "flex",
              flexDirection: "row",
              width: "100%",
            }}
          >
            <IconButton
              color="primary"
              component="label"
              style={{ marginRight: "10px" }}
            >
              <AttachFileIcon />
              <input type="file" hidden onChange={handleFileUpload} />
            </IconButton>
            <TextField
              fullWidth
              variant="outlined"
              label="Type your message"
              value={input}
              onChange={(e) => setInput(e.target.value)}
              onKeyPress={(e) => {
                if (e.key === "Enter") {
                  handleMessageSend();
                }
              }}
            />
          </div>

          <Button
            variant="contained"
            color="primary"
            onClick={handleMessageSend}
            style={{
              whiteSpace: "nowrap",
              height: "56px",
            }}
          >
            Send
          </Button>
        </div>

        <Dialog open={uploading} onClose={() => setUploading(false)}>
          <DialogTitle>Uploading File</DialogTitle>
          <DialogContent>
            <Box sx={{ width: "100%", mt: 2 }}>
              <LinearProgress variant="determinate" value={uploadProgress} />
              <Typography
                variant="body2"
                color="textSecondary"
                style={{ marginTop: "10px" }}
              >
                {`Uploading: ${uploadProgress}%`}
              </Typography>
            </Box>
          </DialogContent>
        </Dialog>
      </Container>

      <style>
        {`
            .wrapper{
                display: flex; 
                gap:20px;
                flex-direction:row;
                align-items: center;
                margin-top: 20px ;
            }

            @media(max-width:767px){
                .wrapper{
                    flex-direction:column;
                }
            }
            `}
      </style>
    </>
  );
};

export default App;
