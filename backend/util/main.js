const fs = require("fs");
const OpenAI = require("openai");
const redis = require("redis");

require("dotenv").config();

const openai = new OpenAI();

const client = redis.createClient({
  url: "redis://localhost:6379",
});

client.on("error", (err) => console.log("Redis Client Error", err));

client.connect();

openai.apiKey = process.env.OPENAI_API_KEY;

const FILE_ID = "file-jPhy1Tmc5AypnkqqHO0vdGG0";

const ASSISTANT_ID = "asst_KNlOuWj8aNGr4TqCHefpVkb0";

const VECTOR_ID = "vs_jMjMtVE77RjfqRkHybnjCV72";

const THREAD_ID = "thread_HAm9F2JT9jhdntiHgmA8x2jL";

const RUN_ID = "run_AKuDB3HndbLsqOHZIMVG9nj0";
// file id: file-jPhy1Tmc5AypnkqqHO0vdGG0

// assistant id: asst_pLRoAALIllOyO73apmI9eZ8X

// vector id: vs_jMjMtVE77RjfqRkHybnjCV72

// thread id: vs_jMjMtVE77RjfqRkHybnjCV72;

// we upload file
const uploadFile = async (filePath) => {
  const file = await openai.files.create({
    file: fs.createReadStream(filePath),
    purpose: "assistants",
  });
  //   console.log(file);
  return file;
};

// we create an assistant

const createAssistantWithFile = async (fileId) => {
  const assistant = await openai.beta.assistants.create({
    name: "Data visualizer",
    description:
      "You are great at creating beautiful data visualizations. You analyze data present in .csv files, understand trends, and come up with data visualizations relevant to those trends. You also share a brief text summary of the trends observed.",
    model: "gpt-4o",
    tools: [{ type: "code_interpreter" }],
    tool_resources: {
      code_interpreter: {
        file_ids: [fileId],
      },
    },
  });
  //   console.log(assistant);
  return assistant;
};

// list files only
async function listFiles() {
  const list = await openai.files.list();

  for await (const file of list) {
    console.log(file);
  }
}

// list assistants
async function listAssistants() {
  const myAssistants = await openai.beta.assistants.list({
    order: "desc",
  });

  console.log(myAssistants.data);
}

// delete assistants
async function deleteAllAssistants() {
  const myAssistants = await openai.beta.assistants.list({
    order: "desc",
    limit: "20",
  });
  for await (const assistant of myAssistants) {
    const response = await openai.beta.assistants.del(assistant.id);

    console.log(response);
  }
}

async function createThread(fileId) {
  const thread = await openai.beta.threads.create({
    messages: [
      {
        role: "user",
        content: "generate analytical summaries or insights ",
        attachments: [
          {
            file_id: fileId,
            tools: [{ type: "code_interpreter" }],
          },
        ],
      },
    ],
  });
  //   console.log(thread);
  return thread;
}

async function createRun() {
  const run = await openai.beta.threads.runs.createAndPoll(THREAD_ID, {
    assistant_id: ASSISTANT_ID,
  });
  console.log(run);
}

const generateAnalyticalData = async (threadId, assistantId) => {
  const run = await openai.beta.threads.runs.createAndPoll(threadId, {
    assistant_id: assistantId,
  });
  const messages = await openai.beta.threads.messages.list(threadId, {
    run_id: run.id,
  });

  const message = messages.data.pop();
  if (message.content[0].type === "text") {
    console.log(message.content);
    const { text } = message.content[0];
    const { annotations } = text;
    const citations = [];

    let index = 0;
    for (let annotation of annotations) {
      text.value = text.value.replace(annotation.text, "[" + index + "]");
      const { file_citation } = annotation;
      if (file_citation) {
        const citedFile = await openai.files.retrieve(file_citation.file_id);
        citations.push("[" + index + "]" + citedFile.filename);
      }
      index++;
    }

    // console.log(text.value);
    // console.log(citations.join("\n"));
    return text.value;
  }
};

async function test(messageText) {
  const threadMessages = await openai.beta.threads.messages.create(THREAD_ID, {
    role: "user",
    content: messageText,
  });
  const run = await openai.beta.threads.runs.createAndPoll(THREAD_ID, {
    assistant_id: ASSISTANT_ID,
  });
  const messages = await openai.beta.threads.messages.list(THREAD_ID, {
    run_id: run.id,
  });

  const message = messages.data.pop();
  if (message.content[0].type === "text") {
    console.log(message.content);
    const { text } = message.content[0];
    const { annotations } = text;
    const citations = [];

    let index = 0;
    for (let annotation of annotations) {
      text.value = text.value.replace(annotation.text, "[" + index + "]");
      const { file_citation } = annotation;
      if (file_citation) {
        const citedFile = await openai.files.retrieve(file_citation.file_id);
        citations.push("[" + index + "]" + citedFile.filename);
      }
      index++;
    }

    console.log(text.value);
    console.log(citations.join("\n"));
  }
}

const uploadHandler = async (filePath) => {
  const uploadedFile = await uploadFile(filePath);
  const assistant = await createAssistantWithFile(uploadedFile.id);
  const thread = await createThread(uploadedFile.id);
  const analyticalResponse = await generateAnalyticalData(
    thread.id,
    assistant.id
  );
  console.log(analyticalResponse);
  client.set("assistantId", assistant.id);
  client.set("threadId", thread.id);

  return analyticalResponse;
};

const messageHandler = async (message) => {
  const assistantId = await client.get("assistantId");
  const threadId = await client.get("threadId");
  const run = await openai.beta.threads.runs.createAndPoll(threadId, {
    assistant_id: assistantId,
  });
  const messages = await openai.beta.threads.messages.list(threadId, {
    run_id: run.id,
  });

  const response = messages.data.pop();
  if (response.content[0].type === "text") {
    console.log(response.content);
    const { text } = response.content[0];
    const { annotations } = text;
    const citations = [];

    let index = 0;
    for (let annotation of annotations) {
      text.value = text.value.replace(annotation.text, "[" + index + "]");
      const { file_citation } = annotation;
      if (file_citation) {
        const citedFile = await openai.files.retrieve(file_citation.file_id);
        citations.push("[" + index + "]" + citedFile.filename);
      }
      index++;
    }

    console.log(text.value);
    return text.value;
  }
};
// uploadFile();

// createAssistantWithFile();

// listFiles();

// listAssistants();

// deleteAllAssistants();

// uploadToVectorStore();

// updateAssistantToUseVector();

// createThread();

// createRun();

// test("please give me analytical summaries or insights");

module.exports = {
  uploadHandler,
  messageHandler,
};
