const express = require("express");
const multer = require("multer");
const path = require("path");
const redis = require("redis");
const cors = require("cors");
const { uploadHandler, messageHandler } = require("./util/main");
require("dotenv").config();

const app = express();

app.use(
  cors({
    origin: "http://localhost:5173",
  })
);
app.use(express.json());

// using redis client we store temp variables like assistant id and thread id
const client = redis.createClient({
  url: "redis://localhost:6379",
});
client.on("error", (err) => console.log("Redis Client Error", err));
client.connect();

const upload = multer({ dest: "uploads/" });
const PORT = 3000;

// upload file api
app.post("/upload", upload.single("file"), async (req, res) => {
  const uploadedFilePath = `./uploads/${req.file.filename}`;

  if (req.file) {
    const response = await uploadHandler(uploadedFilePath);
    res.json(response);
  } else {
    res.status(400).json({ message: "No file uploaded." });
  }
});

// messages api
app.post("/message", async (req, res) => {
  const { message } = req.body;

  if (!message) {
    return res.status(400).json({ error: "Message parameter is required" });
  }

  const messageResponse = await messageHandler(message);
  // Respond to the client
  res.status(200).json({
    message: messageResponse,
  });
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
